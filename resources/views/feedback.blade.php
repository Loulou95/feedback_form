<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0 auto;
            max-width: 1140px;
        }

        .form-section {
            text-align: center;
            margin-top: 20px;
        }
        .v-application--wrap {
            min-height: 0 !important;
        }

    </style>
    <script src="{{ asset('/js/app.js') }}" defer></script>
</head>
<body>
<div class="form-section">
    <div id="app">
        @if( Auth::user())
            <feedback></feedback>
        @endif
    </div>
</div>
</body>
</html>
