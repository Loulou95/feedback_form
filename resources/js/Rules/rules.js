export const Rules = {
    required: [
        v => !!v || 'Please fill in all fields marked *'
    ],

    feedback: [
        v => !!v || 'Please fill in all fields marked *',
        v => (v || '').length <= 1000 || 'Feedback must be less than 1000 characters'
    ],

    postcode: [
        v => !!v || 'Please fill in all fields marked *',
        v => (v || '').length <= 8 || 'Postcode code must be 6-8 digits long',
        v => (v || '').indexOf(' ') < 0 || 'No spaces are allowed',
    ],

    jobNumber: [
        v => !!v || 'Please fill in all fields marked *',
        v => (v || '').length === 9 || 'Job Number must be 9 characters long',
        v => /[JC+][-+][0-9]/.test(v) || 'Job number must start with JC-, followed by 6 digits',
    ],

    phone: [
        v => !!v || 'Please fill in all fields marked *',
        v => /[0-9+]{11,13}/.test(v) || 'Between 11-13 digits required (+, 0-9)'
    ],

};
