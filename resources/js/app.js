/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */




window.Vue = require('vue');
window.axios = require("axios");

import Vuetify from "vuetify";
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'

import Form from "./components/form/index";
import Feedback from "./components/feedback/index";

import { Rules } from './Rules/rules'


Vue.use(Vuetify);
const opts = {}
export default new Vuetify(opts)

Vue.mixin({
    data: function() {
        return {
            get Rules() {
                return Rules;
            }
        }
    }
})

const vuetify = new Vuetify({
    theme: {
        iconfont: 'mdi',
        themes: {
            light: {
                primary: "#707070",
            }
        }
    }
});

const app = new Vue({
    el: '#app',
    vuetify,
    components: {
        "v-form": Form,
        "feedback": Feedback,
    }
});
