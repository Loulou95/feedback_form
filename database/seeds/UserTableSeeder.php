<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id' => 1,
                'email' => 'sam@wiro.com',
                'name' => 'Admin',
                'password' => Hash::make('feedback12?'),
            ],
        ];

        foreach($users as $user) {
            \App\User::updateOrCreate(
                [
                    'id'=>$user['id']
                ],
                $user
            );
        }
    }
}
