<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['namespace' => 'Feedback'], function () {
    Route::post('store', 'FeedbackController@store');
});

Auth::routes();
Route::post('/login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'Feedback'], function () {
        Route::get('get-feedback', 'FeedbackController@getFeedback');
        Route::get('/feedback-results', 'FeedbackController@feedback');
        Route::put('update-status/{id}', 'FeedbackController@updateStatus');
        Route::put('update-rating/{id}', 'FeedbackController@updateRating');
    });
});



Route::get('/home', 'HomeController@index')->name('home');
