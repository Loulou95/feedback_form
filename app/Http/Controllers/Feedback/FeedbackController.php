<?php

namespace App\Http\Controllers\Feedback;

use App\Http\Controllers\Controller;
use App\Http\Requests\Feedback\Store;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FeedbackController extends Controller
{
    public function store(Store $request) {
        $fields = $request->getRequestFields();
        $fields['status_id'] = 1;
        $store = Feedback::create($fields);
        Log::info($store);
    }

    public function getFeedback() {
        $pending = Feedback::where('status', 0)->get();
        $complete = Feedback::where('status', 1)->get();
        return  json_encode(
            [
                'pending' => $pending,
                'complete' => $complete
            ]
        );
    }

    public function updateStatus($id) {
        Feedback::where('id', $id)->update(['status' => 1]);
    }

    public function updateRating(Request $request, $id) {
        $fields = $request->all();
        $feedback =  Feedback::where('id', $id)->first();
        $feedback->update($fields);
    }

    public function feedback() {
        return view('feedback');
    }
    public function dashboard() {
        return view('feedback-app-view');
    }
}
