<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class AbstractRequest extends FormRequest

{
    public function getRequestFields()
    {
        return $this->only(array_keys($this->rules()));
    }
}
