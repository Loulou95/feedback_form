<?php

namespace App\Http\Requests\Feedback;

use App\Http\Requests\AbstractRequest as FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name' => 'required|string|max:100',
            'job_number' => 'required|string',
            'postcode' => 'required|string',
            'telephone' => 'required|string',
            'feedback' => 'required',
            'rating' => 'required|string',
        ];
    }
}
