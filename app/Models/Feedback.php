<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = [
        'customer_name',
        'postcode',
        'telephone',
        'job_number',
        'feedback',
        'rating',
    ];
}
